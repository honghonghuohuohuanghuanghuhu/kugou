import Vue from 'vue'
import Router from 'vue-router'


import HelloWorld from '@/components/HelloWorld'
import Home from '../components/containers/Home/Home.vue'
import NotFound from '../components/containers/NotFound/NotFound.vue'




Vue.use(Router)

export default new Router({
  routes: [
    {path:'/',redirect:'/home'},//重定向
    {
      path:'/home',
      name:'home',
      component:Home
    }
  ]
})
